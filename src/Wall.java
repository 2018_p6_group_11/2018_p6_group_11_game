import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;

public class Wall extends Actor {

	
	public Wall(double a,double b) {
		this.setImage(new Image(getClass().getClassLoader().getResource("resources/wall.png").toString()));
		this.setX(a);
		this.setY(b);
	}
	

	@Override
	public void act(long now) {
		
	}

}
