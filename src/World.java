import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javafx.animation.AnimationTimer;
import javafx.scene.input.KeyCode;

public abstract class World extends javafx.scene.layout.Pane {
	private javafx.animation.AnimationTimer timer;
	public HashSet s = new HashSet();
	
	public World() {
		timer = new AnimationTimer(){
            public void handle(long currentNanoTime){
            	act(currentNanoTime);
               for(int i = 0;i<getObjects(Actor.class).size();i++) {
            	   	getObjects(Actor.class).get(i).act(currentNanoTime);
               }
               
            }
		};
       
}
	

	public abstract void act(long now);
	
	public void add(Actor actor) {
		this.getChildren().add(actor);
	}
	public void addKeyCode(KeyCode code) {
		s.add(code);
	}
	public void removeKeyCode(KeyCode code) {
		s.remove(code);
	}
	public boolean isKeyDown(KeyCode code) {
		if(s.contains(code)) {
			return true;
		}
		return false;
	}
	
	public <A extends Actor> java.util.List<A> getObjects(java.lang.Class<A> cls){
		ArrayList<A> list = new ArrayList<>();
		for(int i = 0;i<this.getChildren().size();i++) {
			if(cls.isAssignableFrom(this.getChildren().get(i).getClass())) {
				list.add((A)this.getChildren().get(i));
			}
		}
		return list;
		

		
	}
	public void remove(Actor actor) {
		this.getChildren().remove(actor);
	} 
	
	
	
	
	
	public void start() {
		timer.start();
	}
	public void stop() {
		timer.stop();
	}
}
