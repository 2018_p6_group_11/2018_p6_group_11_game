import java.util.ArrayList;

public abstract class Actor extends javafx.scene.image.ImageView { 
	
	
	
	public void move(double dx, double dy) {
		this.setX(this.getX()+dx);
		this.setY(this.getY()+dy);
	}
	
	public abstract void act(long now);
	
	public World getWorld() {
		return (World) this.getParent();
	}
	
	public double getWidth() {
		return this.getImage().getWidth();
		}
	public double getHeight() {
		return this.getImage().getHeight();
	}
	
	public <A extends Actor> java.util.List<A> getIntersectingObjects(java.lang.Class<A> cls){
		ArrayList<A> list = new ArrayList<>();
		for(int i = 0;i<this.getWorld().getObjects(cls).size();i++) {
			if(!this.getWorld().getObjects(cls).get(i).equals(this) && this.getWorld().getObjects(cls).get(i).intersects(this.getX(),this.getY(),this.getWidth(),this.getHeight())==true) {
				list.add(this.getWorld().getObjects(cls).get(i));
			}
		}
		return list;
	}
	
	public <A extends Actor> A getOneIntersectingObject(java.lang.Class<A> cls) {
		for(int i = 0;i<this.getWorld().getObjects(cls).size();i++) {
			if(!this.getWorld().getObjects(cls).get(i).equals(this) && this.getWorld().getObjects(cls).get(i).intersects(this.getX(),this.getY(),this.getWidth(),this.getHeight())==true) {
				return this.getWorld().getObjects(cls).get(i);
			}
		}
		return null;
	}
}
