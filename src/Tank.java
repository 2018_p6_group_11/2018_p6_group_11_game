import java.awt.Point;
import java.util.List;
import java.util.Optional;

import javafx.geometry.Point2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

public class Tank extends Actor {

	double dirX = 0;
	double dirY = 1; // the vector of the tanks position or where its facing
	int ms = 7; // movespeed of tank
	int ra = 7;// rotation speed of tank
	KeyCode up; // Keycodes to moving around
	KeyCode down;
	KeyCode left;
	KeyCode right;
	KeyCode fire;
	Ellipse circleFront;
	Ellipse circleBack;
	double frontX;
	double frontY;
	double fX;
	double fY;
	double backX;
	double backY;
	boolean alive = true;
	boolean frontClear;
	boolean backClear;
	Alert alert = new Alert(AlertType.INFORMATION);

	String color;

	public Tank(int i) {
		if (i == 1) {
			this.setImage(new Image(getClass().getClassLoader().getResource("resources/green_tank.png").toString()));
			color = "green";
		} else {
			this.setImage(new Image(getClass().getClassLoader().getResource("resources/blue_tank.png").toString()));
			color = "blue";
		}

	}

	public double getDirX() {
		return dirX;
	}

	public double getDirY() {
		return dirY;
	}

	public void setExplode(Image i) {
		this.setImage(i);
		alive = false;
	}

	public void sethbCoord() {
	}

	public void setKeys(KeyCode a, KeyCode b, KeyCode c, KeyCode d, KeyCode e) {
		up = a;
		down = b;
		left = c;
		right = d;
		fire = e;
	}

	public boolean getAlive() {
		return alive;
	}

	public Ellipse getFrontCircle() {
		return circleFront;
	}

	public Ellipse getBackCircle() {
		return circleBack;
	}

	public void setFront(boolean a) {
		frontClear = a;
	}

	public void setBack(boolean b) {
		backClear = b;
	}

	@Override
	public void act(long now) {

		boolean hitFlag = true;

		if (getAlive() == false) {
			getWorld().stop();
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Game Over");
			alert.setHeaderText("A Tank has been Slain!");
			alert.setContentText("Seems like the " + color + " tank has exploded!");
			alert.show();
			
		}

		System.out.println("DirX = " + dirX);
		System.out.println("DirY = " + dirY);
		System.out.println(" tankAngle= " + (this.getRotate()));
		System.out.println("PosX = " + (this.getX() + this.getWidth()));
		System.out.println("PosY = " + (this.getY() + this.getHeight()));

		dirX = -1 * Math.cos((this.getRotate() + 90) * (Math.PI / 180));
		dirY = Math.sin((this.getRotate() + 90) * (Math.PI / 180));

		frontX = (this.getX() + this.getWidth() / 2) + this.getHeight() * dirX;
		frontY = (this.getY() + this.getHeight() / 2) - this.getWidth() * dirY;

		fX = (this.getX() + this.getWidth() / 2) + this.getHeight() * dirX * 0.75;
		fY = (this.getY() + this.getHeight() / 2) - this.getWidth() * dirY * 0.75;
		backX = (this.getX() + this.getWidth() / 2) - this.getHeight() * dirX * 0.75;
		backY = (this.getY() + this.getHeight() / 2) + this.getWidth() * dirY * 0.75;

		circleFront = new Ellipse(fX, fY, this.getWidth() / 2, 1);
		circleBack = new Ellipse(backX, backY, this.getWidth() / 2, 1);
		circleFront.setOpacity(0);
		circleBack.setOpacity(0);

		getWorld().getChildren().addAll(circleFront, circleBack);

		List<Wall> walls = getWorld().getObjects(Wall.class);
		for (int i = 0; i < walls.size(); i++) {
			Wall wall = walls.get(i);
			if (circleFront.intersects(wall.getX(), wall.getY(), wall.getWidth(), wall.getHeight())) {
				this.setFront(false);
				hitFlag = false;
			}
			if (circleBack.intersects(wall.getX(), wall.getY(), wall.getWidth(), wall.getHeight())) {
				this.setBack(false);
				hitFlag = false;
			}
		}

		if (hitFlag == true) {
			this.setFront(true);
			this.setBack(true);
		}

		if (getWorld().isKeyDown(up) && frontClear == true) {
			this.setX(this.getX() + dirX * ms);
			this.setY(this.getY() - dirY * ms);

		}
		if (getWorld().isKeyDown(down) && backClear == true) {
			this.setX(this.getX() - dirX * ms);
			this.setY(this.getY() + dirY * ms);

		}
		if (getWorld().isKeyDown(left)) {
			this.setRotate(this.getRotate() - ra);

		}
		if (getWorld().isKeyDown(right)) {
			this.setRotate(this.getRotate() + ra);

		}
		if (getWorld().isKeyDown(fire)) {
			Shell shell = new Shell();
			shell.setDX(getDirX() * shell.getMS());
			shell.setDY(-1 * getDirY() * shell.getMS());
			shell.setX(frontX);
			shell.setY(frontY);
			getWorld().getChildren().add(shell);
			getWorld().removeKeyCode(fire);

		}

	}

}
