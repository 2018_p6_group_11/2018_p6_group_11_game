import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Game extends Application {

	int count = 0;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		stage.setTitle("TankMania");
		Menu game = new Menu("Game");
		MenuItem newGame = new MenuItem("New Game");
		MenuBar menuBar = new MenuBar();
		newGame.setOnAction((new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				try {
					start(stage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}));
		MenuItem exit = new MenuItem("Exit");
		exit.setOnAction((new EventHandler<ActionEvent>() {
			public void handle(ActionEvent t) {
				System.exit(0);

			}
		}));
		game.getItems().addAll(newGame, exit);
		menuBar.getMenus().addAll(game);

		BorderPane root = new BorderPane();
		root.setTop(menuBar);
		Map map = new Map(1);
		int[][] arr = map.getMap();

		map.setPrefSize(880, 830);
		root.setCenter(map);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				if (arr[i][j] == 1) {

					map.getChildren().add(new Wall(i * 83, j * 88));
				}
			}
		}
		Scene s = new Scene(root);

		Tank tank1 = new Tank(1);
		Tank tank2 = new Tank(2);
		tank1.setKeys(KeyCode.UP, KeyCode.DOWN, KeyCode.LEFT, KeyCode.RIGHT, KeyCode.SPACE);
		tank2.setKeys(KeyCode.W, KeyCode.S, KeyCode.A, KeyCode.D, KeyCode.SHIFT);
		tank1.setX(250);
		tank1.setY(250);
		tank2.setX(250);
		tank2.setY(500);

		map.getChildren().addAll(tank1, tank2);

		// for(int i = 0;i<arr.length;i++) {
		// for(int j = 0;j<arr[0].length;j++) {
		// if(arr[i][j]==1) {
		// count++;
		// }
		// }
		// }

		// Wall wall = new Wall();
		// wall.setX(0);
		// wall.setY(0);
		// map.getChildren().add(wall);
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Welcome to TankMania!");
		alert.setHeaderText("Controls and Objective");
		alert.setContentText("Player 1 Controls are Arrow Keys and Space for shoot, Player 2 Controls are WASD and shift for shoot."+"\n Your goal is to destroy the other tank without destroying yourself.");
		alert.showAndWait();
		map.start();
		map.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.LEFT) {
					map.addKeyCode(KeyCode.LEFT);
				}
				if (event.getCode() == KeyCode.RIGHT) {
					map.addKeyCode(KeyCode.RIGHT);
				}
				if (event.getCode() == KeyCode.UP) {
					map.addKeyCode(KeyCode.UP);
				}
				if (event.getCode() == KeyCode.DOWN) {
					map.addKeyCode(KeyCode.DOWN);
				}
				if (event.getCode() == KeyCode.A) {
					map.addKeyCode(KeyCode.A);
				}
				if (event.getCode() == KeyCode.D) {
					map.addKeyCode(KeyCode.D);
				}
				if (event.getCode() == KeyCode.W) {
					map.addKeyCode(KeyCode.W);
				}
				if (event.getCode() == KeyCode.S) {
					map.addKeyCode(KeyCode.S);
				}
				if (event.getCode() == KeyCode.SPACE) {
					map.addKeyCode(KeyCode.SPACE);
				}
				if (event.getCode() == KeyCode.SHIFT) {
					map.addKeyCode(KeyCode.SHIFT);
				}
			}
		});

		map.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.LEFT) {
					map.removeKeyCode(KeyCode.LEFT);
				}
				if (event.getCode() == KeyCode.RIGHT) {
					map.removeKeyCode(KeyCode.RIGHT);
				}
				if (event.getCode() == KeyCode.UP) {
					map.removeKeyCode(KeyCode.UP);
				}
				if (event.getCode() == KeyCode.DOWN) {
					map.removeKeyCode(KeyCode.DOWN);
				}
				if (event.getCode() == KeyCode.A) {
					map.removeKeyCode(KeyCode.A);
				}
				if (event.getCode() == KeyCode.D) {
					map.removeKeyCode(KeyCode.D);
				}
				if (event.getCode() == KeyCode.W) {
					map.removeKeyCode(KeyCode.W);
				}
				if (event.getCode() == KeyCode.S) {
					map.removeKeyCode(KeyCode.S);
				}
				if (event.getCode() == KeyCode.SPACE) {
					map.removeKeyCode(KeyCode.SPACE);
				}
				if (event.getCode() == KeyCode.SHIFT) {
					map.removeKeyCode(KeyCode.SHIFT);
				}
			}
		});
		stage.setScene(s);

		
		stage.show();
		stage.setResizable(false);
		map.requestFocus();
		
	}

}
