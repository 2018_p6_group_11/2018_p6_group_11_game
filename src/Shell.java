import javafx.scene.image.Image;

public class Shell extends Actor {
	private double dx = 0;
	private double dy = 0;
	int ms = 15;

	public Shell() {
		this.setImage(new Image(getClass().getClassLoader().getResource("resources/bullet.png").toString()));

	}

	public void setDX(double x) {
		dx = x;
	}

	public void setDY(double y) {
		dy = y;
	}

	public int getMS() {
		return ms;
	}

	@Override
	public void act(long now) {

		if (this.getX() <= 0 || (this.getX() + this.getWidth() >= this.getWorld().getWidth())) {
			dx = -dx;
		}
		if (this.getY() <= 0 || (this.getY() + this.getHeight() >= this.getWorld().getHeight())) {
			dy = -dy;
		}
		if (this.getOneIntersectingObject(Wall.class) != null) {
			Wall wall = this.getOneIntersectingObject(Wall.class);
			if (this.getX() < (wall.getWidth() + wall.getX()) && this.getX() > wall.getX()) {
				dy = -dy;

			}
			if ((this.getY() + this.getHeight() / 2) < (wall.getY() + wall.getHeight())
					&& (this.getHeight() + this.getY()) > wall.getY()) {
				System.out.println("help");
				dx = -dx;

			} else {
				dx = -dx;
				dy = -dy;
			}

		}
		if (this.getOneIntersectingObject(Tank.class) != null) {
			Tank tank = this.getOneIntersectingObject(Tank.class);
			tank.setExplode(new Image("explosion.png"));
		}
		if (this.getOneIntersectingObject(Shell.class) != null) {
			getWorld().remove(this);
			getWorld().remove(this.getOneIntersectingObject(Shell.class));
		}
		this.move(dx, dy);

	}

}
